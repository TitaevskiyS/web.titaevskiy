import random
import MySQLdb
from ask.models import *


def random_strings(num):
	string = ''
	j = 0
	while j < num:
		char = chr(random.randint(97, 122))
		string = string + char
		j += 1
	return string


def random_email():
	string = ''
	string += random_strings(6)
	string +='@'
	string += random_strings(5)
	string += ".com"
	return string


def user(max_count):
	count = 0
	while count < max_count :
		n = random_strings(8)		#name
		e = random_email()			#email
		p = random_strings(10)		#pass
		
		User.objects.create_user(n, e, p)
		count += 1


def tag(max_count):
	count = 0
	while count < max_count :
		n = random_strings(8)		#name
		
		t = Tag(name = n)
		t.save()
		count += 1


def question(max_count):
	count = 0
	while count < max_count :
		t = random_strings(25) + '?'		#title
		q = random_strings(50)
		user = User.objects.get(pk = random.randint(30, 10000))

		quest = Question(title = t, question = q, author = user)
		quest.save()

		tag1 = Tag.objects.get(pk = random.randint(1, 10000))
		tag2 = Tag.objects.get(pk = random.randint(1, 10000))
		tag3 = Tag.objects.get(pk = random.randint(1, 10000))
		
		quest.tags.add(tag1, tag2, tag3)

		count += 1


def answer(max_count):
	count = 0
	while count < max_count :
		a = random_strings(50)
		q = Question.objects.get(pk = random.randint(1, 100000))
		user = User.objects.get(pk = random.randint(30, 10000))

		ans = Answer(question = q, answer = a, author = user)
		ans.save()

		count += 1