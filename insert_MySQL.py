import random
import MySQLdb


def random_strings(num):
	string = ''
	j = 0
	while j < num:
		char = chr(random.randint(97, 122))
		string = string + char
		j += 1
	return string


def random_email():
	string = ''
	string += random_strings(6)
	string +='@'
	string += random_strings(5)
	string += ".com"
	return string


def user(max_count):
	db = MySQLdb.connect(host = "localhost", user = "ask_user", passwd = "ask_pass", db = "ask_db", charset = 'utf8')
	cursor = db.cursor()
	count = 0
	while count < max_count :
		n = random_strings(8)		#name
		e = random_email()			#email
		p = random_strings(10)		#pass
		r = random.randint(0, 300)	#raitng

		sql = "INSERT INTO ask_user (username, email, password, register_date, last_login, rating) \
			VALUES ('%(name)s', '%(email)s', '%(pass)s', NOW(), NOW(), %(r)d)" \
			%{"name":n, "email":e, "pass":p, "r":r}

		cursor.execute(sql)
		db.commit()

		count += 1
	db.close()


def answer(max_count):
	db = MySQLdb.connect(host = "localhost", user = "ask_user", passwd = "ask_pass", db = "ask_db", charset = 'utf8')
	cursor = db.cursor()
	count = 0
	while count < max_count :
		q = random.randint(1, 100000)
		a = random_strings(50)
		author_id = random.randint(30, 10000)

		sql = "INSERT INTO ask_answer (question_id, answer, author_id, right_answer, pub_date, rating) \
			VALUES (%(q_id)d, '%(ans)s', %(a_id)d, 0, NOW(), 0)" \
			%{"q_id":q, "ans":a, "a_id":author_id}

		cursor.execute(sql)
		

		count += 1
	db.commit()
	db.close()