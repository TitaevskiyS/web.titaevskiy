from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager


class UserManager(BaseUserManager):
    def create_user(self, username, email, password):
        if not username:
            raise ValueError('Users must have an username')

        if not email:
            raise ValueError('Users must have an email address')

        if not password:
            raise ValueError('Users must have a password')

        user = self.model(
            username=username,
            email=UserManager.normalize_email(email)
        )

        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, email, password):
        user = self.create_user(
            username,
            email,
            password
        )
        user.is_admin = True
        user.save()
        return user


class User(AbstractBaseUser):
    username = models.CharField(
        max_length=32,
        unique=True
    )
    email = models.EmailField(
        verbose_name='email address',
        max_length=255,
        unique=True
    )
    register_date = models.DateTimeField(
        verbose_name="registration date",
        auto_now_add=True
    )
    avatar = models.ImageField(
        upload_to='avatar/%Y/%m/%d',
        null=True
    )
    rating = models.IntegerField(default=0)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = ['email', 'password']

    objects = UserManager()

# Large spike for admin
    @property
    def is_staff(self):
        return self.username == 'Gexogen'

    @property
    def is_superuser(self):
        return self.username == 'Gexogen'

    def has_module_perms(self, app_label):
        return self.username == 'Gexogen'

    def has_perm(self, perm, obj=None):
        return self.username == 'Gexogen'

    def get_full_name(self):
        return self.username

    def get_short_name(self):
        return self.username
# End of spike


class Tag(models.Model):
    name = models.CharField(max_length=20)

    def __unicode__(self):
        return self.name


class Question(models.Model):
    title = models.CharField(max_length=50)
    question = models.TextField()
    pub_date = models.DateTimeField(
        verbose_name="ask date",
        auto_now_add=True
    )
    author = models.ForeignKey(
        User,
        related_name='questions',
        related_query_name='question'
    )
    tags = models.ManyToManyField(
        Tag,
        null=True,
        related_name='questions',
        related_query_name='question'
    )
    rating = models.IntegerField(default=0)
    views = models.IntegerField(default=0)

    def __unicode__(self):
        return self.title


class Answer(models.Model):
    question = models.ForeignKey(
        Question,
        related_name='answers',
        related_query_name='answer'
    )
    answer = models.TextField()
    author = models.ForeignKey(
        User,
        related_name='answers',
        related_query_name='answer'
    )
    right_answer = models.BooleanField(
        verbose_name='Right',
        default=False
    )
    pub_date = models.DateTimeField(
        verbose_name="answer date",
        auto_now_add=True
    )
    rating = models.IntegerField(default=0)

    def __unicode__(self):
        return self.answer