from django.core.management import BaseCommand
from ask.models import User
from ask_titaevskiy.settings import rel

__author__ = 'gexogen'


class Command(BaseCommand):
    def handle(self, *args, **options):
        users = User.objects.all().order_by('-rating')[:10]

        f = open(rel('../templates') + "/popular_users.html", "w")

        for user in users:
            f.write("<a href='/user_questions/%d' class='list-group-item'>\n" % user.pk)
            f.write("   %s\n" % user.username)
            f.write("   <span class='badge'> %d </span>\n" % user.rating)
            f.write("</a>\n")

        f.close()