from django.core.management import BaseCommand
from django.db.models import Count
from ask.models import Tag
from ask_titaevskiy.settings import STATIC_ROOT

__author__ = 'gexogen'


class Command(BaseCommand):
    def handle(self, *args, **options):
        tags = Tag.objects.annotate(questions_count=Count('question')).order_by('-questions_count')[:20]

        f = open(STATIC_ROOT + "/tags.js", "w")

        f.write("var word_list = [\n")
        for tag in tags:
            f.write("  {text: '%s', weight: %d, link:'/tag/%d'},\n" % (tag.name, tag.questions_count, tag.pk))
        f.write("];")

        f.close()