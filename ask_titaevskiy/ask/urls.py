from django.conf.urls import patterns, url
from ask import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^tag/(?P<tag_id>\d+)/$', views.tag_view, name='tag_view'),
    url(r'^(?P<question_id>\d+)/$', views.detail, name='detail'),
    url(r'^new_question/$', views.new_question, name='new_question'),
    url(r'^login/$', 'django.contrib.auth.views.login', name='login'),
    url(r'^logout/$', 'django.contrib.auth.views.logout', name='logout'),
    url(r'^signin/$', views.signin, name='signin'),
    url(r'^user_info/$', views.user_info, name='user_info'),
    url(r'^user_questions/(?P<user_id>\d+)/$', views.user_questions, name='user_questions'),
)