from django import forms
from django.forms import ModelForm
from ask.models import User, Tag, Question, Answer


def get_tag_or_create(tag):
    if Tag.objects.filter(name=tag).exists():
        return Tag.objects.get(name=tag)
    else:
        res = Tag(name=tag)
        res.save()
        return res


class QuestionForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.author_name = kwargs.pop('username', None)
        if args:
            self.tags = args[0].get('tags')
        super(QuestionForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(QuestionForm, self).save(commit=False)
        instance.author = User.objects.get(username=self.author_name)
        tags = self.tags.split(' ')[:3]
        if commit:
            instance.save()
            for tag in tags:
                instance.tags.add(get_tag_or_create(tag))
        return instance

    class Meta:
        model = Question
        fields = ['title', 'question']


class AnswerForm(ModelForm):
    def __init__(self, *args, **kwargs):
        self.question_id = kwargs.pop('question_id', None)
        self.author_name = kwargs.pop('username', None)
        super(AnswerForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        instance = super(AnswerForm, self).save(commit=False)
        instance.question = Question.objects.get(pk=self.question_id)
        instance.author = User.objects.get(username=self.author_name)
        if commit:
            instance.save()
        return instance

    class Meta:
        model = Answer
        fields = ['answer', ]


class RegisterForm(ModelForm):
    def __init__(self, *args, **kwargs):
        if args:
            self.username = args[0].get('username')
            self.email = args[0].get('email')
            self.password = args[0].get('password')
        super(RegisterForm, self).__init__(*args, **kwargs)

    def save(self, commit=True):
        if commit:
            User.objects.create_user(username=self.username, email=self.email, password=self.password)

    class Meta:
        model = User
        fields = ['username', 'email', 'password', ]


class SettingsForm(forms.Form):
    username = forms.CharField(max_length=32, required=False)
    email = forms.EmailField(max_length=255, required=False)
    avatar = forms.ImageField(required=False)