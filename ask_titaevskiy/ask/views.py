from django.contrib.auth.decorators import login_required
from django.core.mail import send_mail
from django.db import models
from django.shortcuts import render, get_object_or_404, get_list_or_404
from django.http import HttpResponseRedirect
from django.core.paginator import  EmptyPage, PageNotAnInteger, Paginator

from ask.models import Question, Tag, Answer, User
from ask.forms import AnswerForm, QuestionForm, RegisterForm, SettingsForm


def paginator(page, in_list, count):
    pagin = Paginator(in_list, count)

    try:
        res = pagin.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        res = pagin.page(1)
    except EmptyPage:
        # If page is out of range, deliver last page of results.
        res = pagin.page(pagin.num_pages)

    return res


def index(request):
    order = request.GET.get('sort', 'new')
    page = request.GET.get('page', 1)

    if order == 'popular':
        latest_quest_list = Question.objects.all().order_by('-rating', '-pub_date')
    else:
        order = 'new'
        latest_quest_list = Question.objects.all().order_by('-pub_date')

    question = paginator(page, latest_quest_list, 20)

    return render(request, 'index.html', {'quest_list': question,
                                          'order': order,
                                          })


def detail(request, question_id):
    if request.method == 'POST':
        if 'right' in request.POST:
            answer_id = request.POST.get('right')
            Answer.objects.filter(pk=answer_id).update(right_answer=True)
            User.objects.filter(answer__id=answer_id).update(rating=models.F('rating') + 3)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'wrong' in request.POST:
            answer_id = request.POST.get('wrong')
            Answer.objects.filter(pk=answer_id).update(right_answer=False)
            User.objects.filter(answer__id=answer_id).update(rating=models.F('rating') - 3)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'question_like' in request.POST:
            Question.objects.filter(pk=question_id).update(rating=models.F('rating') + 1)
            User.objects.filter(question__id=question_id).update(rating=models.F('rating') + 5)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'question_dislike' in request.POST:
            Question.objects.filter(pk=question_id).update(rating=models.F('rating') - 1)
            User.objects.filter(question__id=question_id).update(rating=models.F('rating') - 5)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'answer_like' in request.POST:
            answer_id = request.POST.get('answer_like')
            Answer.objects.filter(pk=answer_id).update(rating=models.F('rating') + 1)
            User.objects.filter(answer__id=answer_id).update(rating=models.F('rating') + 1)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'answer_dislike' in request.POST:
            answer_id = request.POST.get('answer_dislike')
            Answer.objects.filter(pk=answer_id).update(rating=models.F('rating') - 1)
            User.objects.filter(answer__id=answer_id).update(rating=models.F('rating') - 1)
            return HttpResponseRedirect('/%s/' % question_id)

        elif 'answer' in request.POST:
            form = AnswerForm(request.POST, question_id=question_id, username=request.user.username)
            if form.is_valid():
                form.save()
                send_mail("ASK", "You have an answer\n127.0.0.1/%s/" % question_id, "ASK@gmail.com",
                          [Question.objects.get(pk=question_id).author.email])
                return HttpResponseRedirect('/%s/' % question_id)

    else:
        form = AnswerForm()

    question = get_object_or_404(Question, pk=question_id)
    answer_list = question.answers.all().order_by('-rating', '-pub_date')

    page = request.GET.get('page')
    answers = paginator(page, answer_list, 30)

    Question.objects.filter(pk=question_id).update(views=models.F('views') + 1)

    return render(request, 'detail.html', {'quest': question,
                                           'answers': answers,
                                           'form': form,
                                           })


def tag_view(request, tag_id):
    tag = get_object_or_404(Tag, pk=tag_id)
    question_list = get_list_or_404(Question, tags=tag_id)

    page = request.GET.get('page')
    question = paginator(page, question_list, 20)

    return render(request, 'tag_view.html', {'quest_list': question,
                                             'tag': tag,
                                             })


@login_required
def new_question(request):
    if request.method == 'POST':
        form = QuestionForm(request.POST, username=request.user.username)
        if form.is_valid():
            instance = form.save()
            return HttpResponseRedirect('/%d/' % instance.id)
    else:
        form = QuestionForm()

    return render(request, 'new_question.html', {'form': form}, )


def signin(request):
    if request.method == 'POST':
        form = RegisterForm(request.POST)
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('/login')
    else:
        form = RegisterForm()

    return render(request, 'signin.html', {'form': form}, )


@login_required
def user_info(request):
    if request.method == 'POST':
        form = SettingsForm(request.POST, request.FILES)
        if form.is_valid():
            if form.cleaned_data['email'] and \
                    (not form.cleaned_data['email'] == User.objects.get(username=request.user.username).email):
                User.objects.filter(username=request.user.username).update(email=form.cleaned_data['email'])

            if form.cleaned_data['avatar']:
                user = User.objects.get(username=request.user.username)
                if user.avatar:
                    user.avatar.delete()
                user.avatar = form.cleaned_data['avatar']
                user.save()

            if form.cleaned_data['username'] and \
                    (not form.cleaned_data['username'] == User.objects.get(username=request.user.username).username):
                User.objects.filter(username=request.user.username).update(username=form.cleaned_data['username'])
            return HttpResponseRedirect('/user_info')
    else:
        form = SettingsForm()

    return render(request, 'user_settings.html', {'form': form}, )


def user_questions(request, user_id):
    author = get_object_or_404(User, pk=user_id)
    question_list = Question.objects.filter(author=author)

    page = request.GET.get('page')
    question = paginator(page, question_list, 20)
    return render(request, 'user_questions.html', {'quest_list': question,
                                                   'author': author}, )